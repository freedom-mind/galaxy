import abc
from typing import List


class BaseWorker(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    async def start(self) -> List:
        pass

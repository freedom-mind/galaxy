from typing import Optional
from pydantic import BaseModel


class QueryParams(BaseModel):
    '''параметры передаваемые в запросе'''
    app_id: Optional[str]
    limit: Optional[int]
    offset: int = 0

    def __get_param(self, param_name):
        attr = getattr(self, param_name)
        if attr and isinstance(attr, int):
            return abs(attr)
        return 0

    def get_limit(self):
        return self.__get_param('limit')

    def get_offset(self):
        return self.__get_param('offset')

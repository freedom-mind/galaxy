from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel, Field


class GalaxyStoreReviewList(BaseModel):
    author: str = Field(alias='loginId')
    content: Optional[str] = Field(alias='commentText')
    date: datetime = Field(alias='createDate')
    rate: str = Field(alias='ratingValueNumber')

    def get_rate(self) -> int:
        return int(self.rate.rsplit('-', 1)[-1])

    def get_formatted_date(self):
        return self.date.strftime("%Y-%m-%d %H:%M:%S")

    def to_dict(self):
        return {
            'author': self.author,
            'content': self.content,
            'date': self.get_formatted_date(),
            'rate': self.get_rate(),
        }


class GalaxyStoreReview(BaseModel):
    comment_list: Optional[List[GalaxyStoreReviewList]] = Field(alias='commentList')

    def get_comments(self, limit) -> List:
        if not self.comment_list:
            return []
        data = []
        for count, comment in enumerate(self.comment_list):
            if count >= limit:
                return data
            data.append(comment.to_dict())
        return data


class GalaxyStoreDetailMain(BaseModel):
    content_id: str = Field(alias='contentId')
    content_name: str = Field(alias='contentName')


class GalaxyStoreDetail(BaseModel):
    detail_main: Optional[GalaxyStoreDetailMain] = Field(alias='DetailMain')
    comments_count: int = Field(alias='commentListTotalCount')

    def get_content_id(self) -> Optional[str]:
        if not self.detail_main:
            return
        return self.detail_main.content_id

import asyncio
import random
import aiohttp
from typing import Optional, List
from core.base_worker import BaseWorker
from core.models.galaxy_store import GalaxyStoreDetail, GalaxyStoreReview
from core.models.query_params import QueryParams
from core.user_agents import user_agents


class GalaxyStoreWorker(BaseWorker):
    domain = 'galaxystore.samsung.com'
    reviews_per_page = 15

    def __init__(self, query_params: QueryParams):
        self.query_params = query_params

    def __build_url(self, method) -> str:
        return f'https://{self.domain}/api/{method}'

    async def __request(self, method: str, params: dict = None) -> dict:
        headers = {
            'User-Agent': random.choice(user_agents),
            'Host': self.domain,
            'Referer': f'https://{self.domain}/detail/{self.query_params.app_id}'
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(self.__build_url(method), params=params, headers=headers) as resp:
                if resp.status != 200:
                    return {}
                return await resp.json()

    async def get_app_detail(self) -> Optional[GalaxyStoreDetail]:
        method_url = f'detail/{self.query_params.app_id}'
        result = await self.__request(method_url)
        if not result:
            return
        return GalaxyStoreDetail(**result)

    async def get_comments_list(self, detail: GalaxyStoreDetail) -> List[GalaxyStoreReview]:

        limit = self.query_params.get_limit()
        offset = self.query_params.get_offset()

        if offset + 1 > detail.comments_count:
            raise Exception('offset должен быть меньше количества комментариев')

        data = []
        data_count = 0
        limit = min(limit, detail.comments_count - offset)

        while data_count < limit:
            method_url = f'commentList/contentId={detail.get_content_id()}&startNum={offset + 1}'
            result = await self.__request(method_url)
            comments = GalaxyStoreReview(**result).get_comments(limit - data_count)
            if not comments:
                return data
            data.extend(comments)
            data_count = len(data)
            offset = offset + self.reviews_per_page
            await asyncio.sleep(.3)
        return data

    async def start(self) -> List:
        try:
            detail = await self.get_app_detail()
            if not detail:
                return []
            comments = await self.get_comments_list(detail)
            return comments
        except Exception as e:
            print(e)
        return []

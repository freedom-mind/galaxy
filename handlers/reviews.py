from aiohttp import web
from core.galaxy_store_worker import GalaxyStoreWorker
from core.models.query_params import QueryParams


class ReviewsView(web.View):
    async def get(self):
        params = QueryParams(**self.request.rel_url.query)
        # здесь по хорошему сделать выбор воркера в зависимости от app маркета
        # но задача конкретная, поэтому сразу GalaxyStoreWorker
        reviews = await GalaxyStoreWorker(params).start()

        return web.json_response({'data': reviews})

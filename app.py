from aiohttp import web
from handlers.reviews import ReviewsView



async def init_app():
    app = web.Application()
    app.router.add_view("/reviews", ReviewsView)

    return app


if __name__ == '__main__':
    web.run_app(init_app(), port=80)
